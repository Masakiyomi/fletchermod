//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockState;
//import net.minecraft.block.SoundType;
//import net.minecraft.block.material.Material;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.PlayerInventory;
//import net.minecraft.entity.player.ServerPlayerEntity;
//import net.minecraft.inventory.container.Container;
//import net.minecraft.inventory.container.INamedContainerProvider;
//import net.minecraft.item.BlockItemUseContext;
//import net.minecraft.state.StateContainer;
//import net.minecraft.state.properties.BlockStateProperties;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.ActionResultType;
//import net.minecraft.util.Hand;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.BlockRayTraceResult;
//import net.minecraft.util.text.ITextComponent;
//import net.minecraft.util.text.TranslationTextComponent;
//import net.minecraft.world.IBlockReader;
//import net.minecraft.world.World;
//import net.minecraftforge.fml.network.NetworkHooks;
//
//import javax.annotation.Nullable;
//
//public class DryingRack extends Block{
//
//    public DryingRack() {
//        super(Properties.create(Material.WOOD)
//                .sound(SoundType.WOOD)
//                .hardnessAndResistance(2.5F, 15F)
//                .harvestLevel(0));
//    }
//
//    @Nullable
//    @Override
//    public BlockState getStateForPlacement(BlockItemUseContext context) {
//        return getDefaultState().with(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
//    }
//
//    @Override
//    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
//        builder.add(BlockStateProperties.FACING);
//        builder.add(BlockStateProperties.LIT);
//    }
//
//    @Override
//    public boolean hasTileEntity(BlockState blockState) {
//        return true;
//    }
//
//    @Nullable
//    @Override
//    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
//        return new DryingRackTile();
//    }
//
//    @SuppressWarnings("deprecation")
//    @Override
//    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
//        if (!world.isRemote) {
//            TileEntity tileEntity = world.getTileEntity(pos);
//            if (tileEntity instanceof DryingRackTile) {
//                INamedContainerProvider containerProvider = new INamedContainerProvider() {
//                    @Override
//                    public ITextComponent getDisplayName() {
//                        return new TranslationTextComponent("screen.archeryoverhaulmod.drying_rack");
//                    }
//
//                    @Nullable
//                    @Override
//                    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
//                        return new DryingRackContainer(windowId, playerEntity, playerInventory, world, pos);
//                    }
//                };
//                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) tileEntity, pos);
//            } else{
//                throw new IllegalStateException("Drying Rack INamedContainerProvider is missing!");
//            }
//        }
//        return ActionResultType.SUCCESS;
//    }
//}