//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.inventory.container.ContainerType;
//import net.minecraft.tileentity.TileEntityType;
//import net.minecraftforge.registries.ObjectHolder;
//
//public class ModBlocks {
//    @ObjectHolder("archeryoverhaulmod:bowyers_workbench")
//    public static BowyersWorkbench BOWYERS_WORKBENCH;
//
//    @ObjectHolder("archeryoverhaulmod:bowyers_workbench")
//    public static TileEntityType<BowyersWorkbenchTile> BOWYERS_WORKBENCH_TILE;
//
//    @ObjectHolder("archeryoverhaulmod:bowyers_workbench")
//    public static ContainerType<BowyersWorkbenchContainer> BOWYERS_WORKBENCH_CONTAINER;
//
//    @ObjectHolder("archeryoverhaulmod:drying_rack")
//    public static DryingRack DRYING_RACK;
//
//    @ObjectHolder("archeryoverhaulmod:drying_rack")
//    public static TileEntityType<DryingRackTile> DRYING_RACK_TILE;
//
//    @ObjectHolder("archeryoverhaulmod:drying_rack")
//    public static ContainerType<DryingRackContainer> DRYING_RACK_CONTAINER;
//}