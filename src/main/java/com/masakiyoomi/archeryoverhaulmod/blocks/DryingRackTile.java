//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import com.masakiyoomi.archeryoverhaulmod.items.ModItems;
//import net.minecraft.block.BlockState;
//import net.minecraft.item.ItemStack;
//import net.minecraft.loot.functions.Smelt;
//import net.minecraft.nbt.CompoundNBT;
//import net.minecraft.tileentity.ITickableTileEntity;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.Direction;
//import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.util.INBTSerializable;
//import net.minecraftforge.common.util.LazyOptional;
//import net.minecraftforge.items.CapabilityItemHandler;
//import net.minecraftforge.items.IItemHandler;
//import net.minecraftforge.items.ItemStackHandler;
//
//import javax.annotation.Nonnull;
//import javax.annotation.Nullable;
//
//public class DryingRackTile extends TileEntity implements ITickableTileEntity{
//
//    // Never create lazy optionals in getCapability. Always place them as fields in the tile entity:
//    private ItemStackHandler itemStackHandler = createItemStackHandler();
//    private LazyOptional<IItemHandler> lazyItemStackHandler = LazyOptional.of(() -> itemStackHandler);
//
//    public DryingRackTile() {
//        super(ModBlocks.DRYING_RACK_TILE);
//    }
//
//    @Override
//    public void tick() {
//        if(level.isClientSide) return;
//    }
//
//    private ItemStackHandler createItemStackHandler() {
//        return new ItemStackHandler(1) {
//
//            @Override
//            protected void onContentsChanged(int slot) {
//                // To make sure the TE persists when the chunk is saved later we need to
//                // mark it dirty every time the item handler changes
//                setChanged();
//            }
//
//            @Override
//            public boolean isItemValid(int slot, @Nonnull ItemStack stack)
//            {
//                return ModItems.bowBodyList.contains(stack.getItem());
//            }
//
//            @Nonnull
//            @Override
//            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
//                if (!ModItems.bowBodyList.contains(stack.getItem())){
//                    return stack;
//                }
//                return super.insertItem(slot, stack, simulate);
//            }
//        };
//    }
//
//    /**
//     * Capabilities allow us to add for example power management, or item holding for entities;
//     */
//    @Nonnull
//    @Override
//    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
//        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY){
//            return lazyItemStackHandler.cast();
//        }
//        return super.getCapability(cap, side);
//    }
//
//    @Override
//    public void load(BlockState blockState, CompoundNBT compoundNBT) {
//        CompoundNBT getInventoryCompound = compoundNBT.getCompound("inv");
//        lazyItemStackHandler.ifPresent(h -> ((INBTSerializable<CompoundNBT>) h).deserializeNBT(getInventoryCompound));
//        super.load(blockState, compoundNBT);
//    }
//
//    @Override
//    public CompoundNBT save(CompoundNBT compound) {
//        lazyItemStackHandler.ifPresent(h -> {
//            CompoundNBT serializeNBT = ((INBTSerializable<CompoundNBT>) h).serializeNBT();
//            compound.put("inv", serializeNBT);
//        });
//        return super.save(compound);
//    }
//}