//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockState;
//import net.minecraft.block.SoundType;
//import net.minecraft.block.material.Material;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.PlayerInventory;
//import net.minecraft.entity.player.ServerPlayerEntity;
//import net.minecraft.inventory.container.Container;
//import net.minecraft.inventory.container.INamedContainerProvider;
//import net.minecraft.item.BlockItemUseContext;
//import net.minecraft.state.StateContainer;
//import net.minecraft.state.properties.BlockStateProperties;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.ActionResultType;
//import net.minecraft.util.Hand;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.BlockRayTraceResult;
//import net.minecraft.util.text.ITextComponent;
//import net.minecraft.util.text.TranslationTextComponent;
//import net.minecraft.world.IBlockReader;
//import net.minecraft.world.World;
//import net.minecraftforge.fml.network.NetworkHooks;
//
//import javax.annotation.Nullable;
//
//import net.minecraft.block.AbstractBlock.Properties;
//
//public class BowyersWorkbench extends Block{
//
//    public BowyersWorkbench() {
//        super(Properties.of(Material.WOOD)
//                .sound(SoundType.WOOD)
//                .strength(2.5F, 15F)
//                .harvestLevel(0));
//    }
//
//    @Nullable
//    @Override
//    public BlockState getStateForPlacement(BlockItemUseContext context) {
//        return defaultBlockState().setValue(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
//    }
//
//    @Override
//    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
//        builder.add(BlockStateProperties.FACING);
//    }
//
//    @Override
//    public boolean hasTileEntity(BlockState blockState) {
//        return true;
//    }
//
//    @Nullable
//    @Override
//    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
//        return new BowyersWorkbenchTile();
//    }
//
//    @SuppressWarnings("deprecation")
//    @Override
//    public ActionResultType use(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit) {
//        if (!world.isClientSide) {
//            TileEntity tileEntity = world.getBlockEntity(pos);
//            if (tileEntity instanceof BowyersWorkbenchTile) {
//                INamedContainerProvider containerProvider = new INamedContainerProvider() {
//                    @Override
//                    public ITextComponent getDisplayName() {
//                        return new TranslationTextComponent("screen.archeryoverhaulmod.bowyers_workbench");
//                    }
//
//                    @Nullable
//                    @Override
//                    public Container createMenu(int windowId, PlayerInventory playerInventory, PlayerEntity playerEntity) {
//                        return new BowyersWorkbenchContainer(windowId, playerInventory, pos);
//                    }
//                };
//                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) tileEntity, pos);
//            } else{
//                throw new IllegalStateException("Bowyer's Workbench INamedContainerProvider is missing!");
//            }
//        }
//        return ActionResultType.SUCCESS;
//    }
//}