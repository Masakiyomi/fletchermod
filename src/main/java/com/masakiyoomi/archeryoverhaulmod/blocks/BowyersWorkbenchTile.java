//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.block.BlockState;
//import net.minecraft.nbt.CompoundNBT;
//import net.minecraft.nbt.INBT;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.Direction;
//import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.util.INBTSerializable;
//import net.minecraftforge.common.util.LazyOptional;
//import net.minecraftforge.items.CapabilityItemHandler;
//import net.minecraftforge.items.IItemHandler;
//import net.minecraftforge.items.ItemStackHandler;
//
//import javax.annotation.Nonnull;
//import javax.annotation.Nullable;
//
//public class BowyersWorkbenchTile extends TileEntity{
//    private LazyOptional<IItemHandler> itemStackHandler = LazyOptional.of(this::createItemStackHandler);
//
//    public BowyersWorkbenchTile() {
//        super(ModBlocks.BOWYERS_WORKBENCH_TILE);
//    }
//
//    private ItemStackHandler createItemStackHandler() {
//        return new ItemStackHandler(1) {
//
//        };
//    }
//
//    /**
//     * Capabilities allow us to add for example power management, or item holding for entities;
//     */
//    @Nonnull
//    @Override
//    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
//        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
//            return itemStackHandler.cast();
//        }
//        return super.getCapability(cap, side);
//    }
//
//    @Override
//    public void load(BlockState blockState, CompoundNBT compoundNBT) {
//        CompoundNBT getInventoryCompound = compoundNBT.getCompound("inv");
//        itemStackHandler.ifPresent(h -> ((INBTSerializable<CompoundNBT>) h).deserializeNBT(getInventoryCompound));
//        super.load(blockState, compoundNBT);
//    }
//
//    @Override
//    public CompoundNBT save(CompoundNBT compound) {
//        itemStackHandler.ifPresent(h -> {
//            CompoundNBT serializeNBT = ((INBTSerializable<CompoundNBT>) h).serializeNBT();
//            compound.put("inv", serializeNBT);
//        });
//        return super.save(compound);
//    }
//}