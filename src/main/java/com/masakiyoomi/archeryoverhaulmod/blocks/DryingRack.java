//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockState;
//import net.minecraft.block.CampfireBlock;
//import net.minecraft.block.SoundType;
//import net.minecraft.block.material.Material;
//import net.minecraft.entity.Entity;
//import net.minecraft.item.BlockItemUseContext;
//import net.minecraft.state.StateContainer;
//import net.minecraft.state.properties.BlockStateProperties;
//import net.minecraft.tileentity.CampfireTileEntity;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.world.IBlockReader;
//import net.minecraft.world.World;
//
//import net.minecraft.block.AbstractBlock.Properties;
//
//public class DryingRack extends CampfireBlock {
//
//    public DryingRack(boolean smokey, int fireDamage, Properties properties) {
//        super(smokey, fireDamage, properties);
//    }
//
//    public DryingRack(){
//        super(false, 0 ,
//                Properties.of(Material.WOOD)
//                .sound(SoundType.WOOD)
//                .strength(2.5F, 15F)
//                .harvestLevel(0));
//    }
//
//    @Override
//    public BlockState getStateForPlacement(BlockItemUseContext context) {
//        return defaultBlockState().setValue(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
//    }
//
//    @Override
//    protected void createBlockStateDefinition(StateContainer.Builder<Block, BlockState> builder) {
//        builder.add(BlockStateProperties.FACING);
//        builder.add(BlockStateProperties.LIT);
//    }
//
//    @Override
//    public boolean hasTileEntity(BlockState blockState) {
//        return true;
//    }
//
////    @Override
////    public TileEntity createNewTileEntity(IBlockReader worldIn) {
////        return new CampfireTileEntity();
////    }
//
//    @Override
//    public void entityInside(BlockState state, World worldIn, BlockPos pos, Entity entityIn) {
//        super.entityInside(state, worldIn, pos, entityIn);
//    }
//
//
//}
