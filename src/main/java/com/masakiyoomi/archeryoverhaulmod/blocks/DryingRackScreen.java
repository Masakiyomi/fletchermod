//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod;
//import com.mojang.blaze3d.matrix.MatrixStack;
//import net.minecraft.client.gui.screen.inventory.ContainerScreen;
//import net.minecraft.entity.player.PlayerInventory;
//import net.minecraft.util.ResourceLocation;
//import net.minecraft.util.text.ITextComponent;
//
//public class DryingRackScreen extends ContainerScreen<DryingRackContainer> {
//    private static final ResourceLocation DRYING_RACK_GUI_TEXTURE = new ResourceLocation(ArcheryOverhaulMod.MOD_ID, "textures/gui/container/drying_rack.png");
//
//    public DryingRackScreen(DryingRackContainer container, PlayerInventory inventory, ITextComponent title) {
//        super(container, inventory, title);
//    }
//
//    @Override
//    protected void renderBg(MatrixStack matrixStack, float partialTicks, int x, int y) {
//        this.minecraft.getTextureManager().bind(DRYING_RACK_GUI_TEXTURE);
//    }
//
////    @Override
////    public void render(int mouseX, int mouseY, float partialTicks){
////        this.renderBackground();
////        super.render(mouseX, mouseY, partialTicks);
////        this.renderHoveredToolTip(mouseX, mouseY);
////    }
////
////    @Override
////    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
////        this.font.drawString("Drying rack", 45.0F, 3.0F, 4210752);
////        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 9.0F, (float)(this.ySize/2 + 2), 4210752);
////    }
////
////    @Override
////    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
////        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
////        assert this.minecraft != null;
////        this.minecraft.getTextureManager().bindTexture(DRYING_RACK_GUI_TEXTURE);
////        int i = (this.width - this.xSize) / 2;
////        int j = (this.height - this.ySize) / 2;
////        this.blit(i, j, 0, 0, this.xSize, this.ySize);
////    }
//}
