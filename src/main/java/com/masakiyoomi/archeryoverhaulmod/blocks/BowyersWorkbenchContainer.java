//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.client.Minecraft;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.PlayerInventory;
//import net.minecraft.inventory.CraftResultInventory;
//import net.minecraft.inventory.CraftingInventory;
//import net.minecraft.inventory.container.*;
//import net.minecraft.item.crafting.IRecipe;
//import net.minecraft.item.crafting.RecipeBookCategory;
//import net.minecraft.item.crafting.RecipeItemHelper;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.IWorldPosCallable;
//import net.minecraft.util.math.BlockPos;
//import net.minecraftforge.items.CapabilityItemHandler;
//import net.minecraftforge.items.IItemHandler;
//import net.minecraftforge.items.SlotItemHandler;
//import net.minecraftforge.items.wrapper.InvWrapper;
//
//public class BowyersWorkbenchContainer extends RecipeBookContainer<CraftingInventory>{
//private CraftingInventory craftingInventory = new CraftingInventory(this, 69, 160);
//private CraftResultInventory craftResultInventory = new CraftResultInventory();
//private IWorldPosCallable iWorldPosCallable;
//private final PlayerEntity playerEntity;
//private InvWrapper playerInventory;
//private TileEntity tileEntity;
//
//    public BowyersWorkbenchContainer(int windowId, PlayerInventory playerInventory, BlockPos blockPos) {
//        super(ModBlocks.BOWYERS_WORKBENCH_CONTAINER, windowId);
//        this.iWorldPosCallable = IWorldPosCallable.create(Minecraft.getInstance().level, blockPos);
//        tileEntity = Minecraft.getInstance().level.getBlockEntity(blockPos);
//        this.playerEntity = playerInventory.player;
//        this.playerInventory = new InvWrapper(playerInventory);
//        if (tileEntity != null){
//            tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
//                for (int i = 0; i < 9; i++) {
//                    for (int j = 0; j < 3; j++){
//                        addSlot(new SlotItemHandler(h, i, 26 + j * 18, 17 + i * 18));
//                    }
//                }
//            });
//        }
//        this.addSlot(new CraftingResultSlot(playerEntity, craftingInventory, craftResultInventory, 0, 133, 85));
//        layoutPlayerInventorySlots(8, 196);
//    }
//
//    private int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
//        for (int i = 0 ; i < amount ; i++) {
//            addSlot(new SlotItemHandler(handler, index, x, y));
//            x += dx;
//            index++;
//        }
//        return index;
//    }
//
//    private int addSlotBox(IItemHandler handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy) {
//        for (int j = 0 ; j < verAmount ; j++) {
//            index = addSlotRange(handler, index, x, y, horAmount, dx);
//            y += dy;
//        }
//        return index;
//    }
//
//    private void layoutPlayerInventorySlots(int leftCol, int topRow) {
//        // Player inventory
//        addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);
//
//        // Hotbar
//        topRow += 58;
//        addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
//    }
//
//    @Override
//    public void fillCraftSlotsStackedContents(RecipeItemHelper itemHelperIn) {
//
//    }
//
//    @Override
//    public void clearCraftingContent() {
//
//    }
//
//    @Override
//    public boolean recipeMatches(IRecipe<? super CraftingInventory> recipeIn) {
//        return recipeIn.matches(craftingInventory, playerEntity.level);
//    }
//
//    @Override
//    public int getResultSlotIndex() {
//        return 0;
//    }
//
//    @Override
//    public int getGridWidth() {
//        return this.craftingInventory.getWidth();
//    }
//
//    @Override
//    public int getGridHeight() {
//        return this.craftingInventory.getHeight();
//    }
//
//    @Override
//    public int getSize() {
//        return this.craftingInventory.getContainerSize();
//    }
//
//    @Override
//    public RecipeBookCategory getRecipeBookType() {
//        return RecipeBookCategory.CRAFTING;
//    }
//
//    @Override
//    public boolean stillValid(PlayerEntity playerEntity) {
//        return stillValid(iWorldPosCallable, playerEntity, ModBlocks.BOWYERS_WORKBENCH);
//    }
//}
