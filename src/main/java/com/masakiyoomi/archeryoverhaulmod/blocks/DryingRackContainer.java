//package com.masakiyoomi.archeryoverhaulmod.blocks;
//
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.PlayerInventory;
//import net.minecraft.inventory.CraftingInventory;
//import net.minecraft.inventory.container.Container;
//import net.minecraft.inventory.container.CraftingResultSlot;
//import net.minecraft.inventory.container.FurnaceResultSlot;
//import net.minecraft.inventory.container.Slot;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.IWorldPosCallable;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.world.World;
//import net.minecraftforge.items.CapabilityItemHandler;
//import net.minecraftforge.items.IItemHandler;
//import net.minecraftforge.items.SlotItemHandler;
//import net.minecraftforge.items.wrapper.InvWrapper;
//
//public class DryingRackContainer extends Container {
//    private TileEntity tileEntity;
//    private BlockPos blockPos;
//    private World world;
//    private PlayerEntity playerEntity;
//    private InvWrapper playerInventory;
//    private CraftingInventory craftingInventory = new CraftingInventory(this,98,200);
//    public DryingRackContainer(int windowId, PlayerEntity playerEntity, PlayerInventory inventory, World world, BlockPos blockPos) {
//        super(ModBlocks.DRYING_RACK_CONTAINER, windowId);
//        tileEntity = world.getBlockEntity(blockPos);
//        this.blockPos = blockPos;
//        this.world = world;
//        this.playerEntity = playerEntity;
//        this.playerInventory = new InvWrapper(inventory);
//
//        tileEntity.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
//            addSlot(new Slot(craftingInventory,1,46,13));
//            addSlot(new Slot(craftingInventory,2,46,39));
//            addSlot(new Slot(craftingInventory,3,46,65));
////            addSlot(new SlotItemHandler(h, 0, 46, 39));
////            addSlot(new SlotItemHandler(h, 0, 46, 65));
//
//            addSlot(new FurnaceResultSlot(playerEntity, craftingInventory, 0,46, 13));
//            addSlot(new FurnaceResultSlot(playerEntity, craftingInventory, 1,46, 39));
//            addSlot(new FurnaceResultSlot(playerEntity, craftingInventory, 2,46, 65));
////            addSlot(new CraftingResultSlot(playerEntity, craftingInventory, playerInventory.getInv(),0, 46, 10));
////            addSlot(new CraftingResultSlot(playerEntity, craftingInventory, playerInventory.getInv(),0, 46, 36));
////            addSlot(new CraftingResultSlot(playerEntity, craftingInventory, playerInventory.getInv(),0, 46, 62));
//        });
//
//        layoutPlayerInventorySlots(10, 96);
//    }
//
//    private int addSlotRange(IItemHandler handler, int index, int x, int y, int amount, int dx) {
//        for (int i = 0 ; i < amount ; i++) {
//            addSlot(new SlotItemHandler(handler, index, x, y));
//            x += dx;
//            index++;
//        }
//        return index;
//    }
//
//    private int addSlotBox(IItemHandler handler, int index, int x, int y, int horAmount, int dx, int verAmount, int dy) {
//        for (int j = 0 ; j < verAmount ; j++) {
//            index = addSlotRange(handler, index, x, y, horAmount, dx);
//            y += dy;
//        }
//        return index;
//    }
//
//    private void layoutPlayerInventorySlots(int leftCol, int topRow) {
//        // Player inventory
//        addSlotBox(playerInventory, 9, leftCol, topRow, 9, 18, 3, 18);
//
//        // Hotbar
//        topRow += 58;
//        addSlotRange(playerInventory, 0, leftCol, topRow, 9, 18);
//    }
//
//    @Override
//    public boolean stillValid(PlayerEntity playerIn) {
//        return stillValid(IWorldPosCallable.create(world, blockPos), playerEntity, ModBlocks.DRYING_RACK);
//    }
//}
