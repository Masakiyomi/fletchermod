package com.masakiyoomi.archeryoverhaulmod;

//import com.masakiyoomi.archeryoverhaulmod.blocks.*;
import com.masakiyoomi.archeryoverhaulmod.items.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("archeryoverhaulmod") // The value here should match an entry in the META-INF/mods.toml file
public class ArcheryOverhaulMod {
    public static final String MOD_ID = "archeryoverhaulmod";
    private static final Minecraft MC = Minecraft.getInstance();
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID); // Direct reference to log4j logger.
    public static ArcheryOverhaulMod instance;

    public static ItemGroup archeryOverhaulModGroup = new ItemGroup("archeryoverhaulmod") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ModBows.oak_bow);
        }
    };

    public ArcheryOverhaulMod() {  // Register the setup method for modloading
        instance = this;
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
        init();
        MinecraftForge.EVENT_BUS.register(EventBusHandlers.class);
    }


    private void setup(final FMLCommonSetupEvent event) { // some preinit code
    }

    private void clientRegistries(final FMLClientSetupEvent event) {
//        ScreenManager.register(ModBlocks.BOWYERS_WORKBENCH_CONTAINER, BowyersWorkbenchScreen::new);
//        ScreenManager.register(ModBlocks.DRYING_RACK_CONTAINER, DryingRackScreen::new);
//        RenderingRegistry.registerEntityRenderingHandler(new EntityType.Builder<ModArrowsEntities>(), ModArrowsRenderer::new);
    }

    public void init() {
    }

    public static ResourceLocation resourceLocation(String name) {
        return new ResourceLocation(MOD_ID, name);
    }
}