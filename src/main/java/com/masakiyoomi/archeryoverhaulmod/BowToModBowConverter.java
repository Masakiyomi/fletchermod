//package com.masakiyoomi.archeryoverhaulmod;
//
//import com.google.gson.JsonObject;
//import com.masakiyoomi.archeryoverhaulmod.items.ModBows;
//import net.minecraft.item.Item;
//import net.minecraft.item.ItemStack;
//import net.minecraft.loot.LootContext;
//import net.minecraft.loot.conditions.ILootCondition;
//import net.minecraft.util.JSONUtils;
//import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
//import net.minecraftforge.common.loot.LootModifier;
//import net.minecraftforge.registries.ForgeRegistries;
//
//import javax.annotation.Nonnull;
//import java.util.List;
//import java.util.Random;
//
//public class BowToModBowConverter extends LootModifier {
//    private final Item itemToCheck;
//
//    /**
//     * Constructs a LootModifier.
//     *
//     * @param conditionsIn the ILootConditions that need to be matched before the loot is modified.
//     */
//    protected BowToModBowConverter(ILootCondition[] conditionsIn, Item itemToCheck) {
//        super(conditionsIn);
//        this.itemToCheck = itemToCheck;
//    }
//
//    /**
//     * Additional conditions can be checked, though as much as possible should be parameterized via JSON data.
//     * It is better to write a new ILootCondition implementation than to do things here.
//     */
//    @Nonnull
//    @Override
//    public List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
//        for (ItemStack stack : generatedLoot) {
//            if (stack.getItem().equals(itemToCheck)) {
//
//                Random random = new Random();
//                int bowRoll = random.nextInt(100);
//
////                if (bowRoll < 15) stack = new ItemStack(ModBows.spruce_bow, 1);
////                else if (bowRoll < 40) stack = new ItemStack(ModBows.oak_bow, 1);
////                else if (bowRoll < 60) stack = new ItemStack(ModBows.birch_bow, 1);
////                else if (bowRoll < 80) stack = new ItemStack(ModBows.dark_oak_bow, 1);
////                else if (bowRoll < 92) stack = new ItemStack(ModBows.acacia_bow, 1);
////                else if (bowRoll < 96) stack = new ItemStack(ModBows.bamboo_bow, 1);
////                else stack = new ItemStack(ModBows.jungle_bow, 1);
//
//                generatedLoot.remove(stack);
//                if (bowRoll < 15) generatedLoot.add(new ItemStack(ModBows.spruce_bow, 1));
//                else if (bowRoll < 40) generatedLoot.add(new ItemStack(ModBows.oak_bow, 1));
//                else if (bowRoll < 60) generatedLoot.add(new ItemStack(ModBows.birch_bow, 1));
//                else if (bowRoll < 80) generatedLoot.add(new ItemStack(ModBows.dark_oak_bow, 1));
//                else if (bowRoll < 92) generatedLoot.add(new ItemStack(ModBows.acacia_bow, 1));
//                else if (bowRoll < 96) generatedLoot.add(new ItemStack(ModBows.bamboo_bow, 1));
//                else generatedLoot.add(new ItemStack(ModBows.jungle_bow, 1));
//            }
//        }
//        return generatedLoot;
//    }
//
//    static class Serializer extends GlobalLootModifierSerializer<BowToModBowConverter> {
//
//        @Override
//        public BowToModBowConverter read(ResourceLocation name, JsonObject object, ILootCondition[] conditionsIn) {
//            Item bowItem = ForgeRegistries.ITEMS.getValue(new ResourceLocation((JSONUtils.getAsString(object, "bowItem"))));
//            return new BowToModBowConverter(conditionsIn, bowItem);
//        }
//
//        @Override
//        public JsonObject write(BowToModBowConverter instance) {
//            JsonObject json = makeConditions(instance.conditions);
//            json.addProperty("bowItem", ForgeRegistries.ITEMS.getKey(instance.itemToCheck).toString());
//            return json;
//        }
//    }
//}
//
