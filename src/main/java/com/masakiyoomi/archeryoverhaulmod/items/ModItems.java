package com.masakiyoomi.archeryoverhaulmod.items;

import com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod;
import net.minecraft.item.Item;

import java.util.ArrayList;

import net.minecraft.item.Item.Properties;

public class ModItems extends Item {
    private int breakability;
    public static ArrayList<ModItems> bowBodyList = new ArrayList<>();
    public static ModItems drenched_spruce_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_oak_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_birch_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_dark_oak_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_acacia_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_bamboo_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);
    public static ModItems drenched_jungle_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), true);

    public static ModItems spruce_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems oak_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems birch_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems dark_oak_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems acacia_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems bamboo_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems jungle_bow_body = new ModItems(new Item.Properties().stacksTo(1).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));

    public static ModItems oak_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 20);
    public static ModItems dark_oak_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 18);
    public static ModItems acacia_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 15);
    public static ModItems bamboo_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 12);
    public static ModItems birch_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 12);
    public static ModItems spruce_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 10);
    public static ModItems jungle_arrow_shaft = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 10);

    public static ModItems flint_broadhead = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 10);
    public static ModItems steel_broadhead = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 5);
    public static ModItems golden_broadhead = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 60);
    public static ModItems diamond_broadhead = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 70);

    public static ModItems fletching = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));
    public static ModItems bowstring = new ModItems(new Item.Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup));

    public ModItems(Properties properties) {
        super(properties);
    }

    public ModItems(Properties properties, int breakability) {
        super(properties);
        this.breakability = breakability;
    }

    public ModItems(Properties properties, boolean isBowBody) {
        super(properties);
        if (isBowBody)
            bowBodyList.add(this);
    }

    public int getBreakability(){
        return this.breakability;
    }
}