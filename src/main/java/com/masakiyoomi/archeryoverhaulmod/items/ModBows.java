package com.masakiyoomi.archeryoverhaulmod.items;

import com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod;
import com.masakiyoomi.archeryoverhaulmod.entities.ModArrowsEntities;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.*;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ModBows extends BowItem {
    private float drawSpeedMultiplier;
    public static ModBows spruce_bow = new ModBows(new Properties().durability(270).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.2F);
    public static ModBows oak_bow = new ModBows(new Properties().durability(530).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.2F);
    public static ModBows birch_bow = new ModBows(new Properties().durability(710).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.2F);
    public static ModBows dark_oak_bow = new ModBows(new Properties().durability(840).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.2F);
    public static ModBows acacia_bow = new ModBows(new Properties().durability(920).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.2F);
    public static ModBows bamboo_bow = new ModBows(new Properties().durability(1080).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.4F);
    public static ModBows jungle_bow = new ModBows(new Properties().durability(1250).setNoRepair().tab(ArcheryOverhaulMod.archeryOverhaulModGroup), 1.4F);

    public ModBows(Properties builder, float drawSpeedMultiplier) {
        super(builder);
        this.drawSpeedMultiplier = drawSpeedMultiplier;
    }

    private static ItemStack findAmmo(PlayerEntity player) {
        if (player.getItemInHand(Hand.MAIN_HAND).getItem() instanceof ModArrows) {
            ArcheryOverhaulMod.LOGGER.info("Found arrows in main hand (" + player.getItemInHand(Hand.MAIN_HAND).getItem().getDescription() + ")");
            return player.getItemInHand(Hand.MAIN_HAND);
        } else {
            if (player.getItemInHand(Hand.OFF_HAND).getItem() instanceof ModArrows) {
                ArcheryOverhaulMod.LOGGER.info("Found arrows in off hand (" + player.getItemInHand(Hand.OFF_HAND).getItem().getDescription() + ")");
                return player.getItemInHand(Hand.OFF_HAND);
            } else {
                for (int i = 0; i <= player.inventory.getContainerSize(); ++i) {
                    ItemStack checkedItemStack = player.inventory.getItem(i);
                    if (checkedItemStack.getItem() instanceof ModArrows) {
                        ArcheryOverhaulMod.LOGGER.info("Found arrows in equipment (" + checkedItemStack.getItem().getDescription() + ")");
                        return checkedItemStack;
                    }
                }
            }
        }

        if (player.abilities.instabuild) {
            ArcheryOverhaulMod.LOGGER.info("Player is in creative mode and the equipment/hands don't hold any ammo, using Steel Broadhead Oak Arrow");
            return new ItemStack(ModArrows.oak_arrow_steel_broadhead);
        }

        ArcheryOverhaulMod.LOGGER.info("Haven't found any arrows");
        return ItemStack.EMPTY;
    }

    /**
     * Called when the player stops using an Item (stops holding the right mouse button).
     */
    @Override
    public void releaseUsing(ItemStack usedBow, World worldIn, LivingEntity livingEntity, int timeLeft) {
        if (livingEntity instanceof PlayerEntity) {
            PlayerEntity playerEntity = (PlayerEntity) livingEntity;
            boolean isCreativeModeOn = playerEntity.abilities.instabuild;
            ItemStack itemStack = findAmmo(playerEntity);

            int charge = (int) ((getUseDuration(usedBow) - timeLeft) * getDrawSpeedMultiplier(usedBow));
            charge = net.minecraftforge.event.ForgeEventFactory.onArrowLoose(usedBow, worldIn, playerEntity, charge, !itemStack.isEmpty() || isCreativeModeOn);
            if (charge < 0) return;

            if (!itemStack.getStack().isEmpty()) {
                float arrowVelocity = getArrowVelocity(charge);
                if ((double) arrowVelocity >= 0.1D) {
                    if (!worldIn.isClientSide) {
                        ModArrows modArrowItem = (ModArrows) itemStack.getItem();
                        ModArrowsEntities abstractArrowEntity = (ModArrowsEntities) modArrowItem.createArrow(worldIn, itemStack, playerEntity);
                        customArrow(abstractArrowEntity);

                        if (arrowVelocity >= 1.0F)
                            abstractArrowEntity.setCritArrow(true);

                        usedBow.hurtAndBreak(1, playerEntity, (bow) -> {
                            bow.broadcastBreakEvent(playerEntity.getUsedItemHand());
                        });

                        abstractArrowEntity.shootFromRotation(playerEntity, playerEntity.xRot, playerEntity.yRot, 0.0F, arrowVelocity * 3.0F, 1.0F);
                        worldIn.addFreshEntity(abstractArrowEntity);
                        worldIn.playSound(null, playerEntity.xOld, playerEntity.yOld, playerEntity.zOld, SoundEvents.ARROW_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.0F / (random.nextFloat() * 0.4F + 1.2F) + arrowVelocity * 0.5F);

                        if (!isCreativeModeOn) {
                            itemStack.shrink(1);
                            if (itemStack.isEmpty())
                                playerEntity.inventory.removeItem(itemStack);
                        } else
                            abstractArrowEntity.pickup = AbstractArrowEntity.PickupStatus.DISALLOWED;

                    }

                    playerEntity.awardStat(Stats.ITEM_USED.get(this));
                }
            }
        }
    }

    /**
     * Gets the velocity of the arrow entity from the bow's charge
     */
    public static float getArrowVelocity(int charge) {
        float f = (float) charge / 20.0F;
        f = (f * f + f * 2.0F) / 3.0F;

        if (f > 1.0F)
            f = 1.0F;

        return f;
    }

    public float getDrawSpeedMultiplier(ItemStack stack) {
        ModBows usedBow = (ModBows)stack.getItem();
        return usedBow.drawSpeedMultiplier;
    }

    /**
     * How long it takes to use or consume an item
     */
    @Override
    public int getUseDuration(ItemStack stack) {
        return 72000;
    }


    /**
     * Called to trigger the item's "innate" right click behavior. To handle when this item is used on a Block, see
     */
    @Override
    public ActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
        ItemStack usedBow = player.getItemInHand(hand);
        ItemStack ammo = findAmmo(player);
        boolean hasAmmo = !ammo.getStack().isEmpty();

        ActionResult<ItemStack> ret = net.minecraftforge.event.ForgeEventFactory.onArrowNock(usedBow, world, player, hand, hasAmmo);
        if (ret != null) return ret;

        if (!player.abilities.instabuild && hasAmmo) {
            player.startUsingItem(hand);
            return new ActionResult<>(ActionResultType.SUCCESS, usedBow);
        } else if (player.abilities.instabuild) {
            player.startUsingItem(hand);
            return new ActionResult<>(ActionResultType.SUCCESS, usedBow);
        } else return new ActionResult<>(ActionResultType.FAIL, usedBow);
    }

    @Override
    public boolean isEnchantable(ItemStack stack) {
        return false;
    }

    @Override
    public AbstractArrowEntity customArrow(AbstractArrowEntity arrow) {
        return super.customArrow(arrow);
    }
}