package com.masakiyoomi.archeryoverhaulmod.items;

import com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod;
import com.masakiyoomi.archeryoverhaulmod.entities.ModArrowsEntities;
import com.masakiyoomi.archeryoverhaulmod.entities.ModArrowsRenderer;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.*;
import net.minecraft.world.World;

import net.minecraft.item.Item.Properties;

public class ModArrows extends ArrowItem {
    public static ModArrows oak_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.oak_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows oak_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.oak_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows oak_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.oak_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows oak_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.oak_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows dark_oak_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.dark_oak_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows dark_oak_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.dark_oak_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows dark_oak_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.dark_oak_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows dark_oak_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.dark_oak_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows acacia_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.acacia_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows acacia_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.acacia_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows acacia_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.acacia_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows acacia_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.acacia_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows birch_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.birch_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows birch_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.birch_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows birch_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.birch_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows birch_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.birch_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows spruce_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.spruce_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows spruce_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.spruce_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows spruce_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.spruce_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows spruce_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.spruce_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows bamboo_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.bamboo_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows bamboo_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.bamboo_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows bamboo_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.bamboo_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows bamboo_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.bamboo_arrow_shaft, ModItems.diamond_broadhead);

    public static ModArrows jungle_arrow_flint_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.jungle_arrow_shaft, ModItems.flint_broadhead);
    public static ModArrows jungle_arrow_steel_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.jungle_arrow_shaft, ModItems.steel_broadhead);
    public static ModArrows jungle_arrow_golden_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.jungle_arrow_shaft, ModItems.golden_broadhead);
    public static ModArrows jungle_arrow_diamond_broadhead = new ModArrows(new Properties().stacksTo(64).tab(ArcheryOverhaulMod.archeryOverhaulModGroup), ModItems.jungle_arrow_shaft, ModItems.diamond_broadhead);

    private int arrowBreakability;
    private Item usedBroadhead;

    public ModArrows(Properties builder, ModItems arrowShaft, ModItems broadhead) {
        super(builder);
        this.arrowBreakability = arrowShaft.getBreakability() + broadhead.getBreakability();
        this.usedBroadhead = broadhead;
    }

    /**
     * Returned value will be turned into % chance to loose an arrow after causing damage to living entity;
     */
    public int getArrowBreakability() {
        return this.arrowBreakability;
    }

    public Item getUsedBroadhead() {
        return usedBroadhead;
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        ModArrowsEntities modArrowsEntities = new ModArrowsEntities(worldIn, shooter);
        ModArrowsEntities.usedArrows = stack.getItem();
        ModArrowsRenderer.modArrowsEntities = modArrowsEntities;
        return modArrowsEntities;
    }

    protected ItemStack getArrowStack(ModArrows modArrows) {
        return new ItemStack(modArrows.asItem());
    }

    public static int getModArrowDamage(Item modArrows) {
        if (modArrows instanceof ModArrows) {
            Item broadhead = ((ModArrows) modArrows).getUsedBroadhead();
            if (broadhead.equals(ModItems.flint_broadhead)) return 12;
            if (broadhead.equals(ModItems.steel_broadhead)) return 20;
            if (broadhead.equals(ModItems.golden_broadhead)) return 26;
            if (broadhead.equals(ModItems.diamond_broadhead)) return 32;
        }
        return 0;
    }
}