package com.masakiyoomi.archeryoverhaulmod;

//import com.masakiyoomi.archeryoverhaulmod.blocks.*;
import com.masakiyoomi.archeryoverhaulmod.items.ModArrows;
import com.masakiyoomi.archeryoverhaulmod.items.ModBows;
import com.masakiyoomi.archeryoverhaulmod.items.ModItems;
//import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.SkeletonEntity;
import net.minecraft.entity.monster.StrayEntity;
import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.inventory.container.ContainerType;
//import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
//import net.minecraft.tileentity.TileEntityType;
//import net.minecraft.util.ResourceLocation;
//import net.minecraft.util.math.BlockPos;
//import net.minecraftforge.common.extensions.IForgeContainerType;
//import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nonnull;
import java.util.Random;

import static com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod.*;
//import static com.masakiyoomi.archeryoverhaulmod.blocks.ModBlocks.BOWYERS_WORKBENCH;
//import static com.masakiyoomi.archeryoverhaulmod.blocks.ModBlocks.DRYING_RACK;

public class EventBusHandlers {
    private static final Minecraft MC = Minecraft.getInstance();

    /**
     * this is subscribing to the MOD Event Bus for receiving Registry Events
     */
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    static class ModEventBusHandler {
        @SubscribeEvent
        public static void itemRegistry(final RegistryEvent.Register<Item> event) { // register a new item here
//            event.getRegistry().register(new BlockItem(BOWYERS_WORKBENCH, new Item.Properties().tab(archeryOverhaulModGroup)).setRegistryName(resourceLocation("bowyers_workbench")));
//            event.getRegistry().register(new BlockItem(DRYING_RACK, new Item.Properties().tab(archeryOverhaulModGroup)).setRegistryName(resourceLocation("drying_rack")));
            event.getRegistry().registerAll(
                    ModItems.drenched_spruce_bow_body.setRegistryName(resourceLocation("drenched_spruce_bow_body")),
                    ModItems.drenched_oak_bow_body.setRegistryName(resourceLocation("drenched_oak_bow_body")),
                    ModItems.drenched_birch_bow_body.setRegistryName(resourceLocation("drenched_birch_bow_body")),
                    ModItems.drenched_dark_oak_bow_body.setRegistryName(resourceLocation("drenched_dark_oak_bow_body")),
                    ModItems.drenched_acacia_bow_body.setRegistryName(resourceLocation("drenched_acacia_bow_body")),
                    ModItems.drenched_jungle_bow_body.setRegistryName(resourceLocation("drenched_jungle_bow_body")),
                    ModItems.drenched_bamboo_bow_body.setRegistryName(resourceLocation("drenched_bamboo_bow_body")),

                    ModItems.spruce_bow_body.setRegistryName(resourceLocation("spruce_bow_body")),
                    ModItems.oak_bow_body.setRegistryName(resourceLocation("oak_bow_body")),
                    ModItems.birch_bow_body.setRegistryName(resourceLocation("birch_bow_body")),
                    ModItems.dark_oak_bow_body.setRegistryName(resourceLocation("dark_oak_bow_body")),
                    ModItems.acacia_bow_body.setRegistryName(resourceLocation("acacia_bow_body")),
                    ModItems.jungle_bow_body.setRegistryName(resourceLocation("jungle_bow_body")),
                    ModItems.bamboo_bow_body.setRegistryName(resourceLocation("bamboo_bow_body")),

                    ModBows.spruce_bow.setRegistryName(resourceLocation("spruce_bow")),
                    ModBows.oak_bow.setRegistryName(resourceLocation("oak_bow")),
                    ModBows.birch_bow.setRegistryName(resourceLocation("birch_bow")),
                    ModBows.dark_oak_bow.setRegistryName(resourceLocation("dark_oak_bow")),
                    ModBows.acacia_bow.setRegistryName(resourceLocation("acacia_bow")),
                    ModBows.jungle_bow.setRegistryName(resourceLocation("jungle_bow")),
                    ModBows.bamboo_bow.setRegistryName(resourceLocation("bamboo_bow")),

                    ModItems.spruce_arrow_shaft.setRegistryName(resourceLocation("spruce_arrow_shaft")),
                    ModItems.oak_arrow_shaft.setRegistryName(resourceLocation("oak_arrow_shaft")),
                    ModItems.birch_arrow_shaft.setRegistryName(resourceLocation("birch_arrow_shaft")),
                    ModItems.dark_oak_arrow_shaft.setRegistryName(resourceLocation("dark_oak_arrow_shaft")),
                    ModItems.acacia_arrow_shaft.setRegistryName(resourceLocation("acacia_arrow_shaft")),
                    ModItems.jungle_arrow_shaft.setRegistryName(resourceLocation("jungle_arrow_shaft")),
                    ModItems.bamboo_arrow_shaft.setRegistryName(resourceLocation("bamboo_arrow_shaft")),

                    ModItems.flint_broadhead.setRegistryName(resourceLocation("flint_broadhead")),
                    ModItems.steel_broadhead.setRegistryName(resourceLocation("steel_broadhead")),
                    ModItems.golden_broadhead.setRegistryName(resourceLocation("golden_broadhead")),
                    ModItems.diamond_broadhead.setRegistryName(resourceLocation("diamond_broadhead")),

                    ModItems.fletching.setRegistryName(resourceLocation("fletching")),
                    ModItems.bowstring.setRegistryName(resourceLocation("bowstring")),

                    ModArrows.spruce_arrow_flint_broadhead.setRegistryName(resourceLocation("spruce_arrow_flint_broadhead")),
                    ModArrows.spruce_arrow_steel_broadhead.setRegistryName(resourceLocation("spruce_arrow_steel_broadhead")),
                    ModArrows.spruce_arrow_golden_broadhead.setRegistryName(resourceLocation("spruce_arrow_golden_broadhead")),
                    ModArrows.spruce_arrow_diamond_broadhead.setRegistryName(resourceLocation("spruce_arrow_diamond_broadhead")),

                    ModArrows.oak_arrow_flint_broadhead.setRegistryName(resourceLocation("oak_arrow_flint_broadhead")),
                    ModArrows.oak_arrow_steel_broadhead.setRegistryName(resourceLocation("oak_arrow_steel_broadhead")),
                    ModArrows.oak_arrow_golden_broadhead.setRegistryName(resourceLocation("oak_arrow_golden_broadhead")),
                    ModArrows.oak_arrow_diamond_broadhead.setRegistryName(resourceLocation("oak_arrow_diamond_broadhead")),

                    ModArrows.birch_arrow_flint_broadhead.setRegistryName(resourceLocation("birch_arrow_flint_broadhead")),
                    ModArrows.birch_arrow_steel_broadhead.setRegistryName(resourceLocation("birch_arrow_steel_broadhead")),
                    ModArrows.birch_arrow_golden_broadhead.setRegistryName(resourceLocation("birch_arrow_golden_broadhead")),
                    ModArrows.birch_arrow_diamond_broadhead.setRegistryName(resourceLocation("birch_arrow_diamond_broadhead")),

                    ModArrows.dark_oak_arrow_flint_broadhead.setRegistryName(resourceLocation("dark_oak_arrow_flint_broadhead")),
                    ModArrows.dark_oak_arrow_steel_broadhead.setRegistryName(resourceLocation("dark_oak_arrow_steel_broadhead")),
                    ModArrows.dark_oak_arrow_golden_broadhead.setRegistryName(resourceLocation("dark_oak_arrow_golden_broadhead")),
                    ModArrows.dark_oak_arrow_diamond_broadhead.setRegistryName(resourceLocation("dark_oak_arrow_diamond_broadhead")),

                    ModArrows.acacia_arrow_flint_broadhead.setRegistryName(resourceLocation("acacia_arrow_flint_broadhead")),
                    ModArrows.acacia_arrow_steel_broadhead.setRegistryName(resourceLocation("acacia_arrow_steel_broadhead")),
                    ModArrows.acacia_arrow_golden_broadhead.setRegistryName(resourceLocation("acacia_arrow_golden_broadhead")),
                    ModArrows.acacia_arrow_diamond_broadhead.setRegistryName(resourceLocation("acacia_arrow_diamond_broadhead")),

                    ModArrows.jungle_arrow_flint_broadhead.setRegistryName(resourceLocation("jungle_arrow_flint_broadhead")),
                    ModArrows.jungle_arrow_steel_broadhead.setRegistryName(resourceLocation("jungle_arrow_steel_broadhead")),
                    ModArrows.jungle_arrow_golden_broadhead.setRegistryName(resourceLocation("jungle_arrow_golden_broadhead")),
                    ModArrows.jungle_arrow_diamond_broadhead.setRegistryName(resourceLocation("jungle_arrow_diamond_broadhead")),

                    ModArrows.bamboo_arrow_flint_broadhead.setRegistryName(resourceLocation("bamboo_arrow_flint_broadhead")),
                    ModArrows.bamboo_arrow_steel_broadhead.setRegistryName(resourceLocation("bamboo_arrow_steel_broadhead")),
                    ModArrows.bamboo_arrow_golden_broadhead.setRegistryName(resourceLocation("bamboo_arrow_golden_broadhead")),
                    ModArrows.bamboo_arrow_diamond_broadhead.setRegistryName(resourceLocation("bamboo_arrow_diamond_broadhead"))
            );
            LOGGER.info("Items have been registered");
        }

//        @SubscribeEvent
//        public static void blockRegistry(final RegistryEvent.Register<Block> event) {
//            event.getRegistry().registerAll(
//                    new BowyersWorkbench().setRegistryName(resourceLocation("bowyers_workbench")),
//                    new DryingRack().setRegistryName(resourceLocation("drying_rack"))
//            );
//            LOGGER.info("Blocks have been registered");
//        }
//
//        @SubscribeEvent
//        public static void tileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
//            event.getRegistry().register(TileEntityType.Builder.of(DryingRackTile::new, ModBlocks.DRYING_RACK).build(null).setRegistryName(resourceLocation("drying_rack")));
//            event.getRegistry().register(TileEntityType.Builder.of(BowyersWorkbenchTile::new, ModBlocks.BOWYERS_WORKBENCH).build(null).setRegistryName(resourceLocation("bowyers_workbench")));
//            LOGGER.info("TileEntityTypes have been registered");
//        }
//
//        @SubscribeEvent
//        public static void containerRegistry(final RegistryEvent.Register<ContainerType<?>> event) {
//            event.getRegistry().register(IForgeContainerType.create((windowId, inventory, data) -> {
//                BlockPos position = data.readBlockPos();
//                assert MC.player != null;
//                return new BowyersWorkbenchContainer(windowId, MC.player.inventory, position);
//            }).setRegistryName(resourceLocation("bowyers_workbench")));
//
//            event.getRegistry().register(IForgeContainerType.create((windowId, inventory, data) -> {
//                BlockPos position = data.readBlockPos();
//                assert MC.player != null;
//                assert MC.level != null;
//                return new DryingRackContainer(windowId, MC.player, MC.player.inventory, MC.level, position);
//            }).setRegistryName(resourceLocation("drying_rack")));
//
//            LOGGER.info("Containers have been registered");
//        }
    }

    /**
     * this is subscribing to the FORGE Event Bus for receiving Registry Events
     */
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE)
    static class ForgeEventBusHandler {

        @SubscribeEvent
        public static void mobsLoot(final LivingDropsEvent event) {
            Entity entity = event.getEntity();

            if (!(entity instanceof PlayerEntity))
                event.getDrops().remove(Items.BOW);

            if (entity instanceof SkeletonEntity || entity instanceof StrayEntity) {
                event.getDrops().clear();
                Random random = new Random();
                int arrowsRoll = random.nextInt(100);

                Item rolledArrows;
                if (arrowsRoll < 15) rolledArrows = ModArrows.oak_arrow_flint_broadhead;
                else if (arrowsRoll < 45) rolledArrows = ModArrows.dark_oak_arrow_flint_broadhead;
                else if (arrowsRoll < 90) rolledArrows = ModArrows.acacia_arrow_flint_broadhead;
                else if (arrowsRoll < 95) rolledArrows = ModArrows.birch_arrow_flint_broadhead;
                else rolledArrows = ModArrows.bamboo_arrow_flint_broadhead;

                int lootingLevel = event.getLootingLevel();
                int arrowsAmount = random.nextInt(3) + (int) (lootingLevel * 0.6);
                int bonesAmount = random.nextInt(3) + (int) (lootingLevel * 0.6);

                ItemStack arrows = new ItemStack(rolledArrows, arrowsAmount);
                ItemStack bones = new ItemStack(Items.BONE, bonesAmount);
                ItemEntity arrowsDrop = new ItemEntity(entity.getCommandSenderWorld(), entity.xOld, entity.yOld, entity.zOld, arrows);
                ItemEntity bonesDrop = new ItemEntity(entity.getCommandSenderWorld(), entity.xOld, entity.yOld, entity.zOld, bones);

                event.getDrops().add(arrowsDrop);
                event.getDrops().add(bonesDrop);
            }

            LOGGER.info("MobsLoot has been registered");
        }

//        @SubscribeEvent
//        public static void registerModifierSerializers(@Nonnull final RegistryEvent.Register<GlobalLootModifierSerializer<?>> event) {
//            event.getRegistry().register(new BowToModBowConverter.Serializer().setRegistryName(new ResourceLocation("archeryoverhaulmod", "bow_to_mod_bow")));
//        }
    }
}