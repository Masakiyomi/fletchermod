package com.masakiyoomi.archeryoverhaulmod.entities;

import com.masakiyoomi.archeryoverhaulmod.items.ModArrows;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import java.util.Random;

public class ModArrowsEntities extends ArrowEntity {
    public static Item usedArrows;
    public static int arrowDamage;

    public ModArrowsEntities(World world, LivingEntity shooter) {
        super(world, shooter);
    }

    @Override
    public void shoot(double x, double y, double z, float velocity, float inaccuracy) {
        super.shoot(x, y, z, velocity, inaccuracy);
    }

    @Override
    protected void doPostHurtEffects(LivingEntity target) {
        if (!level.isClientSide) {
            ModArrows damagingArrow = (ModArrows) usedArrows;
            arrowDamage = ModArrows.getModArrowDamage(usedArrows);

            if(isCritArrow())
                arrowDamage += 0.2*arrowDamage;

            target.hurt(DamageSource.MAGIC, arrowDamage);
            DamageSource.arrow(this, target);

            if (!target.isAlive()) {
                Random random = new Random();
                for (int i = 0; i < target.getArrowCount(); i++) {
                    if (random.nextInt(100) > damagingArrow.getArrowBreakability())
                        target.spawnAtLocation(damagingArrow);
                }
            }
        }
    }

    /**
     * Called to update the entity's position/logic.
     */
    @Override
    public void tick() {
        super.tick();
    }
}