package com.masakiyoomi.archeryoverhaulmod.entities;

import com.masakiyoomi.archeryoverhaulmod.ArcheryOverhaulMod;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

public class ModArrowsRenderer extends ArrowRenderer<ModArrowsEntities> {
    public static ModArrowsEntities modArrowsEntities;

    public ModArrowsRenderer(EntityRendererManager entityRendererManager) {
        super(entityRendererManager);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(ModArrowsEntities entity) {
        return new ResourceLocation(ArcheryOverhaulMod.MOD_ID, "textures/entity/projectiles/oak_arrow_flint_broadhead.png");
    }

//    @Override
//    protected boolean bindEntityTexture(ModArrowsEntities entity) {
//        this.renderManager.textureManager.bindTexture(new ResourceLocation(ArcheryOverhaulMod.MOD_ID, "textures/entity/projectiles/oak_arrow_flint_broadhead.png"));
//        return true;
//    }
//
//    @Override
//    public void doRender(ModArrowsEntities entity, double x, double y, double z, float entityYaw, float partialTicks) {
//        bindEntityTexture(modArrowsEntities);
//        super.doRender(entity, x, y, z, entityYaw, partialTicks);
//    }
}